<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Place;
use App\Models\PlaceUser;
class PlaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $place = new Place;
        $place->title = 'Új intézmény';
        $place->save();

        $place = new Place;
        $place->title = 'Új intézmény 2';
        $place->save();

        $place = new Place;
        $place->title = 'Új intézmény 3';
        $place->save();

        $role = new PlaceUser();
        $role->place_id = 1;
        $role->user_id = 1;
        $role->level = 3;
        $role->save();

        $role = new PlaceUser();
        $role->place_id = 1;
        $role->user_id = 1;
        $role->level = 3;
        $role->save();

        $role = new PlaceUser();
        $role->place_id = 2;
        $role->user_id = 1;
        $role->level = 3;
        $role->save();

        $role = new PlaceUser();
        $role->place_id = 3;
        $role->user_id = 1;
        $role->level = 3;
        $role->save();
    }
}
