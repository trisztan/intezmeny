<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsLogTable extends Migration {

	public function up()
	{
		Schema::create('payments_log', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('year');
            $table->integer('place_id');
            $table->integer('payment_id');
			$table->string('name')->nullable();
			$table->string('value')->nullable();
            $table->string('user')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('payments_log');
	}
}
