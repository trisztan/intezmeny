<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOptionValuesTable extends Migration
{
    public function up()
    {
        Schema::create('option_values', function (Blueprint $table) {
            $table->increments('id');
            $table->string('option_id');
            $table->string('payment_id');
            $table->string('value');
            $table->integer('year');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('option_values');
    }
}
