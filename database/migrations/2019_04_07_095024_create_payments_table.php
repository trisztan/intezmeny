<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	public function up()
	{
		Schema::create('payments', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('year');
			$table->string('name')->nullable();
			$table->string('jan')->nullable();
			$table->string('feb')->nullable();
			$table->string('mar')->nullable();
			$table->string('apr')->nullable();
			$table->string('maj')->nullable();
			$table->string('jun')->nullable();
			$table->string('jul')->nullable();
			$table->string('aug')->nullable();
			$table->string('szept')->nullable();
			$table->string('okt')->nullable();
			$table->string('nov')->nullable();
			$table->string('dec')->nullable();
			$table->integer('place_id');
            $table->integer('is_year')->nullable();
			$table->timestamps();
			$table->integer('last_edit');
			$table->tinyInteger('old');
		});
	}

	public function down()
	{
		Schema::drop('payments');
	}
}
