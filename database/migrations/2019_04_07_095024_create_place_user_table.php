<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlaceUserTable extends Migration {

	public function up()
	{
		Schema::create('place_user', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('place_id');
			$table->tinyInteger('level');
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('place_user');
	}
}
