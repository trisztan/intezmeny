<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Backend\AdminController@login');
Route::get('login', 'Backend\AdminController@login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/new', 'Auth\ResetPasswordController@new')->name('password.new');


Route::prefix('account')->middleware(['auth','web'])->namespace('Backend')->group(function () {
    Route::get('places', 'PlaceController@place')->name('admin.places');
    Route::get('place/{id}', 'PlaceController@selectPlace')->name('admin.place');
});

Route::prefix('account')->middleware(['auth','web','place'])->namespace('Backend')->group(function () {
    Route::get('/', 'AdminController@index')->name('home.index');
    Route::post('table/year', 'AdminController@year')->name('home.year');
    Route::post('table/changeyear', 'AdminController@yearChange')->name('home.yearChange');
    Route::post('table/update', 'AdminController@update')->name('home.update');
    Route::post('table/create', 'AdminController@create')->name('home.create');
    Route::post('table/change', 'AdminController@changeOld')->name('home.change');
    Route::post('table/delete', 'AdminController@delete')->name('home.delete');
    Route::post('table/show', 'AdminController@show')->name('home.show');
    Route::post('table/log', 'AdminController@log')->name('home.log');
    Route::get('settings', 'SettingController@show')->name('settings.index');

    Route::post('settings/create/user', 'UserController@createUser')->name('user.createUser');
    Route::post('settings/update/user', 'UserController@updateUser')->name('user.createUpdate');
    Route::post('settings/delete/user', 'UserController@deleteUser')->name('user.createDelete');

    Route::post('settings/create/place', 'PlaceController@createPlace')->name('place.createPlace');
    Route::post('settings/update/place', 'PlaceController@updatePlace')->name('place.updatePlace');
    Route::post('settings/delete/place', 'PlaceController@deletePlace')->name('place.deletePlace');

    Route::post('settings/order/option', 'OptionController@orderOption')->name('option.orderOption');
    Route::post('settings/delete/option', 'OptionController@deleteOption')->name('option.deleteOption');
    Route::post('settings/update/option', 'OptionController@updateOption')->name('option.updateOption');
    Route::post('settings/create/option', 'OptionController@createOption')->name('option.createOption');

    Route::post('settings/delete/value', 'OptionController@deleteValue')->name('option.deleteValue');
    Route::post('settings/update/value', 'OptionController@updateValue')->name('option.updateValue');
    Route::post('settings/create/value', 'OptionController@createValue')->name('option.createValue');
});
