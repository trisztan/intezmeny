const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery', 'easy-autocomplete', 'easyAutocomplete'],
    Dropzone: ['Dropzone'],
    toastr: ['toastr'],
    chart: ['chart'],
    sortable: ['sortable'],
    select2: ['select2'],
    editable: ['editable'],
});

mix.js([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js',
        './node_modules/toastr/build/toastr.min.js',
        './node_modules/select2/dist/js/select2.full.min.js',
        './node_modules/sortablejs/Sortable.min.js',
        './node_modules/x-editable-bs4/dist/bootstrap4-editable/js/bootstrap-editable.min.js',
        './node_modules/jquery-freeze-table/dist/js/freeze-table.js',
        './node_modules/js-cookie/src/js.cookie.js',
        './resources/backend/js/app.js'],
    'public/backend/js/app.js')
    .sass('resources/backend/sass/app.scss', 'public/backend/css/').version();

mix.js([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/icheck/icheck.min.js',
        './resources/backend/js/login.js'],
    'public/backend/js/login.js').version();

mix.browserSync({
    host: 'intezmeny.loc',
    proxy: {
        target: "http://intezmeny.loc",
        ws: true
    },
    port: 9166
});
