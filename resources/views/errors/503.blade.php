@extends('errors::illustrated-layout')

@section('code', '503')
@section('title', __('Karbantartás'))

@section('message', __($exception->getMessage() ?: __('Bocs, Az oldal karbantartás alatt áll. Ez maximum 5 percet vesz igénybe.')))
