@extends('errors::illustrated-layout')

@section('code', '401')
@section('title','401 Hiba')


@section('message', __('Bocs, az oldal megtekintéséhez nincs jogosultságod.'))
