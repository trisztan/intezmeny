@extends('errors::illustrated-layout')

@section('code', '404')
@section('title', __('A keresett oldal nem található.'))

@section('message', __('A keresett oldal nem található.'))
