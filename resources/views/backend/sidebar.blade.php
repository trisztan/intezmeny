<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
        data-accordion="false">
        <li class="nav-item">
            <a href="{{route('users.index')}}" class="nav-link">
                <i class="nav-icon fa fa-users"></i>
                <p>
                    Felhasználók
                    <span class="badge badge-info right">{{\App\Models\User::count()}}</span>
                </p>
            </a>
        </li>
        <hr/>com
        <li class="nav-header">Beállítások</li>
        <li class="nav-item">
            <a href="{{route('settings.index')}}" class="nav-link">
                <i class="nav-icon fa fa-file"></i>
                <p>Beállítások</p>
            </a>
        </li>
    </ul>
</nav>
