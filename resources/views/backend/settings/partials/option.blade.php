<ul id="items">
@foreach($options as $option)
    <li data-option-id="{{$option->id}}" data-option-order-id="{{$option->order_id}}">
        <span class="float-right"><i class="fa fa-arrows-alt handle"></i></span>
        <div class="row mb-4">
            @if($option->type != 'select')
                <div class="m-3 font-weight-bold">Szöveges mező:</div>
            @else
                <div class="m-3 font-weight-bold">Legördülő mező:</div>
            @endif

            <div class="col-12">
                {!! Form::text('option',$option->option, ['class' => 'form-control', 'id' => 'option-'.$option->id]) !!}
            </div>
            @if($option->type == 'select')

            <div class="col-12">
                <div class="m-3 font-weight-bold">Opciók:</div>
                @php $optionValues = explode(",",$option->values); @endphp
                @foreach($optionValues as $value)
                    @php
                    if(empty($value))
                    {
                        continue;
                    }
                    @endphp
                    <div class="row ml-3 mb-2">
                        <div class="col-8">{!! Form::text('option',$value, ['class' => 'form-control col-6 ml-3', 'id' => 'option-'.$option->id.'-'.$value]) !!}</div>
                        <div class="col-4">
                            <button data-option-value-delete="{{$option->id}}" data-option-value="{{$value}}" class="btn btn-sm btn-danger mt-2">
                                <i class="fa fa-trash"></i> Törlés
                            </button>
                            <button data-option-value-update="{{$option->id}}" data-option-value-old="{{$value}}" data-option-value="option-{{$option->id}}-{{$value}}" class="btn btn-sm btn-success mt-2">
                                <i class="fa fa-save"></i> Mentés
                            </button>
                        </div>
                    </div>
                @endforeach
                <div class="row">
                    <div class="col-12">
                        <div class="row ml-3 mb-2">
                            <div class="col-8">{!! Form::text('option','', ['class' => 'form-control col-6 ml-3', 'id' => 'option-value-'.$option->id]) !!}</div>
                            <div class="col-4">
                                <button data-option-value-create="{{$option->id}}" data-option-value-old="{{$value}}" data-option-value="option-value-{{$option->id}}" class="btn btn-sm btn-primary mt-2">
                                    <i class="fa fa-plus"></i> új hozzáadása
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-4">
                <button data-option-delete="{{$option->id}}" class="btn btn-sm btn-danger mt-2">
                    <i class="fa fa-trash"></i> Törlés
                </button>
                <button data-option-update="{{$option->id}}" class="btn btn-sm btn-success mt-2">
                    <i class="fa fa-save"></i> Mentés
                </button>
            </div>
        </div>
    </li>
@endforeach
</ul>
