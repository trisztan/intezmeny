@php $i=0; @endphp
@foreach($places as $place)
    @php $i++; @endphp
    <div class="row mb-4">
        <div class="col-4">
            {!! Form::text('title',$place->title, ['class' => 'form-control', 'id' => 'place-'.$place->id]) !!}
        </div>
        <div class="col-3">
            @if($i>1 && $place->id != session('place'))
            <button data-place-delete="{{$place->id}}"  class="btn btn-sm btn-danger mt-2">
                <i class="fa fa-trash"></i> Törlés
            </button>
            @endif
            <button data-place-update="{{$place->id}}" class="btn btn-sm btn-success mt-2">
                <i class="fa fa-save"></i> Mentés
            </button>
        </div>
    </div>
@endforeach
