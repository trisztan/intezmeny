 @foreach($placeUsers as $user)
<div class="row mb-4">
   <div class="col-4">
        {!! Form::text('email',$user->email, ['class' => 'form-control', 'id' => 'email-'.$user->id,'disabled'=>'true']) !!}
    </div>
    <div class="col-4">
        {!! Form::select('level',$roles,$user->pivot->level,['class' => 'form-control', 'id' => 'level-'.$user->id]) !!}
    </div>
    <div class="col-4">
        @if($user->is_admin == 0)
        <button data-user-delete="{{$user->id}}" class="btn btn-sm btn-danger mt-2">
            <i class="fa fa-trash"></i> Törlés
        </button>
        <button data-user-update="{{$user->id}}" class="btn btn-sm btn-success mt-2">
            <i class="fa fa-save"></i> Mentés
        </button>
            @endif
    </div>
</div>
@endforeach