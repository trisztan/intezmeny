@extends('backend.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    [{{ \App\Models\Place::find(session('place'))->title }}] felhasználói:
                                </h3>

                                <div class="card-tools">

                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-2">
                                <div class="form-group">
                                    <div class="row mb-2">
                                        <div class="col-4">
                                           E-mail cím:
                                        </div>
                                        <div class="col-2">
                                           Jogosultság:
                                        </div>
                                        <div class="col-4">

                                        </div>
                                    </div>
                                    <div data-user-table>
                                    @include('backend.settings.partials.user',compact('placeUsers','roles'))
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-4">
                                            {!! Form::select('email',$users,null,['id' => 'newemail']) !!}
                                        </div>
                                        <div class="col-4">
                                            {!! Form::select('level',$roles,'',['class' => 'form-control', 'id' => 'newlevel']) !!}
                                        </div>
                                        <div class="col-1">
                                            <button data-user-add class="btn btn-sm btn-primary mt-2">
                                                <i class="fa fa-plus"></i> Új hozzáadása
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="col-6">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Intézmények
                                </h3>

                                <div class="card-tools">

                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-2">
                                <div data-place-table>
                                    @include('backend.settings.partials.place',compact('places'))
                                </div>
                                <div class="row mb-4">
                                    <div class="col-4">
                                        {!! Form::text('title','', ['class' => 'form-control', 'id' => 'newplace']) !!}
                                    </div>
                                    <div class="col-3">
                                        <button data-place-add class="btn btn-sm btn-primary mt-2">
                                            <i class="fa fa-trash"></i> Új hozzadása
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Oszlop beállítások
                                </h3>

                                <div class="card-tools">
                                    <button type="submit" data-option-create="text" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> Új beviteli mező</button>
                                    <button type="submit" data-option-create="select" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> Új legördülő mező</button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-2 col-12">
                                <div data-option-table>
                                    @include('backend.settings.partials.option',compact('options'))
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div><!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
