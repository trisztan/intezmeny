
@extends('backend.frontend')
@section('content')
<div class="md:flex min-h-screen">
    <div class="w-full bg-white flex items-center justify-center">
        <div class="login-box">
            <h2 class="mb-5 text-center">
                Belépés
            </h2>
            <h4 class="mb-5 text-center">
                Üdvözöljük az oldalon!
            </h4>
            <div class="card">
                <div class="card-body login-card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="input-group mb-3">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail cím" required autofocus>
                            <div class="input-group-append">
                                <span class="fa fa-envelope input-group-text"></span>
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="input-group mb-3">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" placeholder="Jelszó" required autofocus>
                            <div class="input-group-append">
                                <span class="fa fa-key input-group-text"></span>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <a href="{{url('password/reset')}}">Elfelejtettem a jelszavam</a>
                            </div>
                            <div class="col-12 text-center mt-4">
                                <button type="submit" class="btn text-center btn-primary btn-block btn-flat">Belépés</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{ mix('/backend/js/login.js') }}"></script>
@endsection
