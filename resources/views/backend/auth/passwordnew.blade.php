@extends('backend.frontend')
@section('content')
    <div class="md:flex min-h-screen">
        <div class="w-full bg-white flex items-center justify-center">
            <div class="login-box">
                <h2 class="mb-5 text-center">
                    Elfelejtett jelszó
                </h2>

                <div class="card">

                    <div class="card-body">
                        <div class="alert alert-success" role="alert">
                           Ön sikeresen állított be új jelszót. Jelentkezzen be!
                        </div>
                        <div class="col-md-12">
                            <div class="col-12 text-center mt-4">
                                <a  href="{{url('/')}}" class="btn text-center btn-primary btn-block btn-flat">Belépés</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
