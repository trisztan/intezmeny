@extends('backend.frontend')
@section('content')
    <div class="md:flex min-h-screen">
        <div class="w-full bg-white flex items-center justify-center">
            <div class="login-box">
                <h2 class="mb-5 text-center">
                    Elfelejtett jelszó
                </h2>
                <h4 class="mb-5 text-center">
                    Adja meg e-mail címét és elküldjük a további teendőket.
                </h4>
                <div class="card">

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="email">Az ön e-mail címe:</label>
                                    <input id="email" type="email" placeholder="Email cím" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">
                                       Küldés
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
