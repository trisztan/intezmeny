@if(count($logs)>0)
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <td>Mező</td>
            <td>Érték</td>
            <td>Mikor</td>
            <td>Ki</td>
        </tr>
        </thead>
        <tbody>
        @foreach($logs as $log)
            <tr>
                <td>{{$log->name}}</td>
                <td>{{$log->value}} </td>
                <td>{{$log->created_at}}</td>
                <td>{{$log->email}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p>Nincs loggolt adat!</p>
@endif
