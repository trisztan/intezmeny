@php
if($type == 'new'){
    $show = \App\Models\Table::whereUserId(Auth::user()->id)->where('year',session('year'))->wherePlaceId(session('place'))->whereType('new')->first()->status ?? 1;
}
else {
    $show = \App\Models\Table::whereUserId(Auth::user()->id)->where('year',session('year'))->wherePlaceId(session('place'))->whereType('old')->first()->status ?? 0;
}

@endphp

<div class="header{{$type}} p-0 h-25 mt-2 rounded-0" data-table-{{$type}}-show="{{$show}}" {{$show == 0 ? 'style=display:none':''}}>
    <div class="card-body p-0" style="height:60px;">
        <div class="col-md-12" style="z-index:100">
            <div class="row">
                <div class="col-md-1 pt-2">
                    <h5>
                       <span data-table-type="1" style="cursor:pointer">
                           @if($type == 'new') <b>Jelenlegiek</b>@else Jelenlegiek @endif  </span>
                    </h5>
                </div>
                <div class="col-md-1 pt-2">
                    <h5>
                       <span data-table-type="0"  style="cursor:pointer">
                         @if($type == 'old') <b>Régiek</b>@else Régiek @endif  </span>
                    </h5>
                </div>
                @if(Auth::user()->rank != 'VIEW')
                    <div class="col-md-2 pt-2">
                        <button class="btn btn-sm btn-primary ml-5"
                                data-table-create="{{$type}}" {{$show == 0 ? 'style=display:none':''}}>
                            <i class="fa fa-plus"></i> Új sor hozzadása
                        </button>
                    </div>
                @endif
                <div class="col-md-4  pt-3" data-table-lastedit="{{$type}}" {{$show == 0 ? 'style=display:none':''}}>
                    @php

                        /*$data = \App\Models\Option::select('option_values.updated_at')->leftJoin('option_values','options.id','=','option_values.option_id')->where('options.place_id',session('place'))->where('option_values.year',session('year'))->orderBy('option_values.updated_at','DESC')->first();*/

                        $lastedited = \App\Models\Payment::where('place_id',session('place'))->where('year',session('year'))->whereOld(($type == 'new' ? 0:1))->orderBy('updated_at','DESC')->first();
                        echo 'Utoljára szerkesztve: ';
                        echo Helper::time_elapsed_string($lastedited->updated_at ?? '') ?? ' ---';
                    @endphp
                </div>
            </div>
        </div>
    </div>
</div>


<thead class="{{$type}}header">
<tr>
    <th style="min-width:100px;text-align:center">Sorsz.</th>
    <th style="min-width:100px;text-align:center">Megnevezés</th>
    @if(count($options)>0)
        @foreach($options as $option)
            <th style="min-width:100px;text-align:center">{{$option->option}}</th>
        @endforeach
    @endif
    <th style="min-width:100px;text-align:center">Január</th>
    <th style="min-width:100px;text-align:center">Február</th>
    <th style="min-width:100px;text-align:center">Március</th>
    <th style="min-width:100px;text-align:center">Április</th>
    <th style="min-width:100px;text-align:center;">Május</th>
    <th style="min-width:100px;text-align:center">Június</th>
    <th style="min-width:100px;text-align:center">Július</th>
    <th style="min-width:100px;text-align:center">Augusztus</th>
    <th style="min-width:100px;text-align:center">Szeptember</th>
    <th style="min-width:100px;text-align:center">Október</th>
    <th style="min-width:100px;text-align:center">November</th>
    <th style="min-width:100px;text-align:center">December</th>
    @if(Auth::user()->rank != 'VIEW')
        <th style="min-width:80px;text-align:center;">Mód.</th>
    @endif

    <th style="min-width:100px;text-align:center">Log</th>
</tr>
</thead>
<tbody>

@php
    $count = 16;
@endphp
@if(count($options)>0)
    @foreach($options as $option)
        @php $count++ @endphp
    @endforeach
@endif

@php $i =0; @endphp
@foreach($payments as $payment)
    @php $i++; @endphp

    <tr class="{{$type}} @if($lastedited->id == $payment->id) trhover @endif">
        <td>{{$i}}</td>
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="name" class="datatext" data-field="name"
                                data-type="text"
                                data-field="Megnevezés"
                                data-pk="{{$payment->id}}"
                                data-title="" @endif>{{$payment->name}}</a></td>
        @if(count($options)>0)
            @foreach($options as $option)
                @php
                    $data = \App\Models\OptionValues::wherePaymentId($payment->id)->where('year',session('year'))->whereOptionId($option->id)->first() ?? '';
                    $fieldtype = ($option->type == 'text' ? 'text':'select');
                    if($fieldtype == 'select'){
                        $source = explode(',',$option->values);
                        $source = array_filter($source);
                        $sourcestring = [];
                        foreach($source as $so) {
                            $sourcestring[] = ['value'=>$so,'text'=>$so];
                        }
                        $source = json_encode($sourcestring);
                    }
                    else {
                        $source = '';
                    }
                @endphp
                <td class="data{{$fieldtype}}"><a @if(Auth::user()->rank != 'VIEW') class="data{{$fieldtype}}"
                                                  id="{{$option->option}}"
                                                  data-field="{{$option->option}}"
                                                  data-option="{{$option->id}}"
                                                  data-source="{{$source}}" data-type="{{$fieldtype}}"
                                                  data-pk="{{$payment->id}}"
                                                  data-title="" @endif>{{$data->value??'0'}}</a></td>
            @endforeach
        @endif
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="jan" class="datatext" data-field="jan"
                                data-type="text"
                                data-field="Január"
                                data-pk="{{$payment->id}}"
                                data-title="" @endif>{{$payment->jan ?? '0'}}</a></td>
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="feb" class="datatext" data-field="feb"
                                data-type="text"
                                data-field="Február"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->feb ?? '0'}}</a></td>
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="mar" class="datatext" data-field="mar"
                                data-type="text"
                                data-field="Március"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->mar ?? '0'}}</a></td>
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="apr" class="datatext" data-field="apr"
                                data-type="text"
                                data-field="Április"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->apr ?? '0'}}</a></td>

        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="maj" class="datatext" data-field="maj"
                                data-type="text"
                                data-field="Május"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->maj ?? '0'}}</a></td>

        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="jun" class="datatext" data-field="jun"
                                data-type="text"
                                data-field="Június"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->jun ?? '0'}}</a></td>
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="jul" class="datatext" data-field="jul"
                                data-type="text"
                                data-field="Július"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->jul ?? '0'}}</a></td>
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="aug" class="datatext" data-field="aug"
                                data-type="text"
                                data-field="Augusztus"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->aug ?? '0'}}</a></td>
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="szept" class="datatext" data-field="szept"
                                data-type="text"
                                data-field="Szeptember"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->szept ?? '0'}}</a></td>
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="okt" class="datatext" data-field="okt"
                                data-type="text"
                                data-field="Október"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->okt ?? '0'}}</a></td>
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="nov" class="datatext" data-field="nov"
                                data-type="text"
                                data-field="November"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->nov ?? '0'}}</a></td>
        <td class="datatext"><a @if(Auth::user()->rank != 'VIEW') id="dec" class="datatext" data-field="dec"
                                data-type="text"
                                data-field="December"
                                data-pk="{{$payment->id}}"
                                data-title=""@endif>{{$payment->dec ?? '0'}}</a></td>
        @if(Auth::user()->rank != 'VIEW')
            <td>
                <button type="button" class="btn btn-sm btn-primary" data-change-old="{{$payment->id}}">
                    <i class="fa fa-sticky-note"></i>
                </button>
                <button type="button" class="btn btn-sm btn-danger" data-table-delete="{{$payment->id}}">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
        @endif
        <td style="font-size:11px;" data-show-log="{{$payment->id}}">
            {{\App\Models\User::find($payment->last_edit)->email ?? ''}}<br/>
            {{$payment->updated_at}}
        </td>
    </tr>

@endforeach
@if($type != 'new')
</tbody>
@endif
