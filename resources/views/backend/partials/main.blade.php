<!-- Navbar -->
<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom fixed-top">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item d-none d-sm-inline-block m-2">
            Üdv, {{ Auth::user()->email }}
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <select data-table-changeyear class="form-control d-inline-block">
                @php
                    $yearsdata = \App\Models\Payment::select('year')
                ->wherePlaceId(session('place'))
                ->groupBy('year')
                ->where('is_year', 1)
                ->get() ?? null;
                @endphp
                @if(count($yearsdata)>0)
                    @foreach($yearsdata as $yeard)
                        <option value="{{$yeard->year}}"
                                @if(session('year') == $yeard->year) selected @endif>{{$yeard->year}}</option>
                    @endforeach
                @endif
            </select>
        </li>
        @if(Auth::user()->rank == 'ADMIN' || Auth::user()->rank == 'EDIT')
            <li class="nav-item d-none d-sm-inline-block">
                <button class="btn btn-sm d-inline-block btn-primary m-1" data-table-year>
                    <i class="fa fa-plus"></i> Év hozzáadása
                </button>
            </li>
        @endif
        <li class="nav-item d-none d-sm-inline-block ml-5">

            @php
                $data = \App\Models\Option::wherePlaceId(session('place'))->whereType('select')->orderBy('order_id','asc')->first() ?? null;
                $i=0;
                if($data != null){
                    $values = explode(',',$data->values);
                    foreach($values as $value) {
                        $value = trim($value);
                        if(empty($value)){

                            continue;
                        }
                        $count = (\App\Models\OptionValues::whereIn('payment_id',\App\Models\Payment::select('id')->where('place_id',session('place'))->whereOld(0)->get())->where('year',session('year'))->whereOptionId($data->id)->where('value',$value)->count() ?? 0);
                        $i = $i+$count;
                        echo '<span class="ml-5">'.$value.': '.$count."</span>";
                    }
                }
            @endphp
            <span class="ml-5"><b>Összesen:</b> {{$i}}</span>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="btn btn-sm btn-primary m-2" data-toggle="dropdown" href="#">
                <i class="fa fa-home"></i> {{ \App\Models\Place::find(session('place'))->title }}</a>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header"><a href="{{url('account/places')}}">Intézmény választó</a></span>
                <div class="dropdown-divider"></div>
                @foreach(\App\Models\User::find(Auth::user()->id)->places as $selectPlace)
                    <a href="{{url('account/place/'.$selectPlace->id)}}" class="dropdown-item">
                        {{$selectPlace->title}} <span class="pull-right"><i class="fa fa-chevron-right"></i></span>
                    </a>
                    <div class="dropdown-divider"></div>
                @endforeach
            </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        @if(Auth::user()->rank == 'ADMIN')
            <li class="nav-item">
                <a href="{{url('account/settings')}}" class="btn btn-sm btn-default m-2"><i class="fa fa-cog"></i>
                    Beállítások</a>
            </li>
        @endif
        @if(Request::is('account/settings'))
            <li class="nav-item">
                <a href="{{url('admin')}}" class="btn btn-sm btn-default m-2"><i class="fa fa-chevron-circle-left"></i>
                    Vissza</a>
            </li>
        @endif
        <li class="nav-item">
            <a class="btn btn-sm btn-danger m-2" href="{{ route('logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-times"></i>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</nav>
<div style="margin-top:150px">
    <table cellspacing="0" id="table-new" class="table table-sm table-bordered table-striped" style="max-height: 90vh;">
        @include('backend.partials.table',['payments'=>$payments,'options'=>$options,'years'=>$years,'type'=>'new'])
    </table>
    <table cellspacing="0" id="table-old" class="table table-sm table-bordered table-striped" style="max-height: 90vh;">
        @include('backend.partials.table',['payments'=>$oldpayments,'options'=>$options,'years'=>$years,'type'=>'old'])
    </table>
    <div class="modal" id="showLog" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmModalLabel">Log</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                </div>

                <div class="modal-footer">
                    <div class="text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Mégse</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
