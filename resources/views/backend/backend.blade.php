<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('app.name')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ mix('backend/css/app.css') }}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <script>
        var authid = '{{Auth::user()->id}}';
    </script>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    @if(Request::is('account/settings'))
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom fixed-top">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item d-none d-sm-inline-block m-2">
                Üdv, {{ Auth::user()->email }}
            </li>
        </ul>

    <!-- Right navbar links -->
   <ul class="navbar-nav ml-auto">
       <li class="nav-item dropdown">
           <a class="btn btn-sm btn-primary m-2" data-toggle="dropdown" href="#">
               <i class="fa fa-home"></i> {{ \App\Models\Place::find(session('place'))->title }}</a>
           </a>
           <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
               <span class="dropdown-item dropdown-header"><a href="{{url('account/places')}}">Intézmény választó</a></span>
               <div class="dropdown-divider"></div>
               @foreach(\App\Models\User::find(Auth::user()->id)->places as $selectPlace)
               <a href="{{url('account/place/'.$selectPlace->id)}}" class="dropdown-item">
                  {{$selectPlace->title}} <span class="pull-right"><i class="fa fa-chevron-right"></i></span>
               </a>
               <div class="dropdown-divider"></div>
             @endforeach
           </div>
       </li>
        <!-- Notifications Dropdown Menu -->
        @if(Auth::user()->rank == 'ADMIN')
       <li class="nav-item">
           <a href="{{url('account/settings')}}" class="btn btn-sm btn-default m-2"><i class="fa fa-cog"></i> Beállítások</a>
       </li>
       @endif
       @if(Request::is('account/settings'))
           <li class="nav-item">
               <a href="{{url('account')}}" class="btn btn-sm btn-default m-2"><i class="fa fa-chevron-circle-left"></i> Vissza</a>
           </li>
       @endif
        <li class="nav-item">
            <a class="btn btn-sm btn-danger m-2" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-times"></i>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
    @endif

<!-- Main Sidebar Container -->

    @yield('content')
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script>
    var RANK = '{{Auth::user()->rank }}';
</script>
<script src="{{ mix('backend/js/app.js') }}"></script>
@yield('js')

</body>
</html>
