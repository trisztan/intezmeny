
@extends('backend.frontend')
@section('content')
<div class="md:flex min-h-screen">
    <div class="w-full bg-white flex items-center justify-center">
        <div class="login-box">
            <h2 class="mb-5 text-center">
               Intézmény választó
            </h2>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <ul class="list-group">
                   @foreach($places as $place)
                            <li class="list-group-item"><a href="{{url('account/place/'.$place->id)}}">{{$place->title}} <span class="pull-right"><i class="fa fa-chevron-right"></i></span></a></li>
                   @endforeach
                    </ul>
                </div>
                <div class="col-12 text-center mb-4">
                    <a class="" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Kilépés
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
