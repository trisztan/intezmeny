@extends('backend.backend')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-top: -90px;">
        <!-- Content Header (Page header) -->
        <div data-table-table>
            @include('backend.partials.main',compact('payments','oldpayments','options','years'))
        </div>

    </div>
@endsection
