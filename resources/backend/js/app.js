require('datatables.net-bs4');
require('datatables.net-fixedcolumns-bs4');
require('datatables.net-fixedheader-bs4');
require('./AdminLTE');
import toastr from 'toastr';
import Sortable from 'sortablejs';

window.toastr = toastr;


var App = function () {
    'use strict';
    $.fn.editable.defaults.mode = 'popup';
    var loader = "<span class='spinner-grow spinner-grow-sm'></span> Kis türelmet...";

    toastr.options = {
        "positionClass": "toast-top-center",
    };

    var setDatatableDefaults = function () {
        $.extend(true, $.fn.dataTable.defaults, {
            oLanguage: {
                "sEmptyTable": "Nincs rendelkezésre álló adat",
                "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
                "sInfoEmpty": "Nulla találat",
                "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
                "sInfoPostFix": "",
                "sInfoThousands": " ",
                "sLengthMenu": "_MENU_ találat oldalanként",
                "sLoadingRecords": "Betöltés...",
                "sProcessing": "Feldolgozás...",
                "sSearch": "Keresés:",
                "sZeroRecords": "Nincs a keresésnek megfelelő találat",
                "oPaginate": {
                    "sFirst": "Első",
                    "sPrevious": "Előző",
                    "sNext": "Következő",
                    "sLast": "Utolsó"
                },
                "oAria": {
                    "sSortAscending": ": aktiválja a növekvő rendezéshez",
                    "sSortDescending": ": aktiválja a csökkenő rendezéshez"
                }
            },
        });
    }
    var showLog = function(){

        $(document).on("click", "[data-show-log]", function () {
            $('#showLog').find('modal-body').empty();
            var id = $(this).attr('data-show-log');

            $.ajax({
                url: '/account/table/log',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id
                },
                cache: false,
                success: function (result) {
                    $('#showLog').find('.modal-body').html(result.html);
                    $('#showLog').modal('show');
                }
            });
        });

    }
    var getDatatableSize = function() {
        var size = '';
        console.log($(window).height());
        if ($(window).height() >= 1000) {
            size = '90vh';
        }
        else if ($(window).height() >= 900) {
            size = '75vh';
        }
        else if ($(window).height() >= 800) {
            size = '70vh';
        }
        else {
           size = '60vh';
        }
        console.log(size);
        return size;
    }
    var dataDatatable = function () {
        if ($('#table-new').length) {
            if ($.fn.DataTable.isDataTable($('#table-new'))) {
                $('#table-new').empty();
                $('#table-new').DataTable().destroy();
            }
            var tablenew = $('#table-new').DataTable({
                "scrollY":       getDatatableSize(),
                "scrollX":        true,
                "scrollCollapse": true,
                "paging": false,
                "order": [[1, "asc"]],
                searching: true,
                stateSave: true,
                responsive: true,
                fixedColumns: {
                    leftColumns: 2
                },
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem(authid + 'DataTables_a' + settings.sInstance, JSON.stringify(data))
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem(authid + 'DataTables_a' + settings.sInstance))
                },
                drawCallback: function (settings) {
                    var hidenew = $('[data-table-new-show]').attr('data-table-new-show');

                    if (hidenew == 0) {
                        $('[data-table-create="new"]').hide();
                        $('[data-table-lastedit="new"]').hide();
                        $('[data-table-search="new"]').hide();
                        setTimeout(function () {
                            $('#table-new_wrapper').css({'opacity': 0, 'height': '0px'});
                            $('#table-new').css({'opacity': 0});
                        }, 500);
                    } else {
                        $('#table-new_wrapper').css({'opacity': 1,'height': '100%'});
                        $('#table-new').css({'opacity': 1});
                    }
                }
            });
        }
        if ($('#table-old').length) {
            if ($.fn.DataTable.isDataTable($('#table-old'))) {
                $('#table-old').empty();
                $('#table-old').DataTable().destroy();
            }
            var tableold = $('#table-old').DataTable({
                scrollY:        getDatatableSize(),
                "scrollX":        true,
                "scrollCollapse": true,
                "paging":false,
                stateSave: true,
                searching: true,
                "order": [[1, "asc"]],
                fixedColumns: {
                    leftColumns: 2
                },
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem(authid + 'DataTables_b' + settings.sInstance, JSON.stringify(data))
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem(authid + 'DataTables_b' + settings.sInstance))
                },
                drawCallback: function (settings) {
                    var hidenew = $('[data-table-old-show]').attr('data-table-old-show');
                    if (hidenew == 0) {
                        $('[data-table-create="old"]').hide();
                        $('[data-table-lastedit="old"]').hide();
                        $('[data-table-search="old"]').hide();
                        setTimeout(function () {
                            $('#table-old_wrapper').css({'opacity': 0, 'height': '0px','z-index':'8000'});
                            $('#table-old').css({'opacity': 0,'z-index':'8000'});
                        }, 500);
                    } else {
                        $('#table-old_wrapper').css({'opacity': 1, 'height': '100%','z-index':'1'});
                        $('#table-old').css({'opacity': 1,'z-index':'1'});
                    }
                }
            });
        }
    }
    var verifyToken = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
    var newemail = function () {
        if ($('#newemail').length) {
            $("#newemail").select2({
                tags: true,
                width: '100%'
            });
        }
    }
    var sortOption = function () {
        if ($('#items').length) {
            var el = document.getElementById('items');

            new Sortable(el, {
                animation: 400,
                ghostClass: 'blue-background-class',
                onSort: function (event) {
                    var sort = [];
                    $("#items > li").each(function (i) {
                        var option = $(this).attr('data-option-id');
                        if (option != null) {
                            sort[i] = $(this).attr('data-option-id');
                        }
                    });
                    $.ajax({
                        url: '/account/settings/order/option',
                        type: "POST",
                        method: "POST",
                        dataType: "json",
                        data: {
                            order: sort
                        },
                        cache: false,
                        success: function (result) {
                            if (result.status == 'success') {
                                toastr.success(result.message);
                            } else {
                                toastr.error(result.message);
                            }
                        }
                    });
                },
            });
        }
    }
    var changeOld = function () {

        $(document).on("click", "[data-change-old]", function () {
            var id = $(this).attr('data-change-old');
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/table/change',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-table-table]').empty();
                        $('[data-table-table]').append(result.data);
                        adattable();

                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });
    }
    var userAdd = function () {

        $(document).on("click", "[data-user-add]", function () {
            var email = $('#newemail').val();
            var level = $('#newlevel').val();
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/create/user',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    email: email,
                    level: level
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-user-table]').empty();
                        $('[data-user-table]').append(result.data);
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });
    }

    var userDel = function () {

        $(document).on("click", "[data-user-delete]", function () {
            var id = $(this).attr('data-user-delete');
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/delete/user',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id,
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-user-table]').empty();
                        $('[data-user-table]').append(result.data);
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });

    }
    var userUpdate = function () {

        $(document).on("click", "[data-user-update]", function () {
            var id = $(this).attr('data-user-update');
            var level = $('#level-' + id).val();
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/update/user',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id,
                    level: level
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-user-table]').empty();
                        $('[data-user-table]').append(result.data);
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });

    }
    var placeAdd = function () {

        $(document).on("click", "[data-place-add]", function () {
            var title = $('#newplace').val();
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/create/place',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    title: title,
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-place-table]').empty();
                        $('[data-place-table]').append(result.data);
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });

    }
    var placeDelete = function () {

        $(document).on("click", "[data-place-delete]", function () {
            var id = $(this).attr('data-place-delete');
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/delete/place',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id,
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-place-table]').empty();
                        $('[data-place-table]').append(result.data);
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });

    }
    var placeUpdate = function () {

        $(document).on("click", "[data-place-update]", function () {
            var id = $(this).attr('data-place-update');
            var title = $('#place-' + id).val();
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/update/place',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id,
                    title: title
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-place-table]').empty();
                        $('[data-place-table]').append(result.data);
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });

    }
    var optionDelete = function () {

        $(document).on("click", "[data-option-delete]", function () {
            var id = $(this).attr('data-option-delete');
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/delete/option',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id,
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-option-table]').empty();
                        $('[data-option-table]').append(result.data);
                        sortOption();
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });

    }
    var optionUpdate = function () {

        $(document).on("click", "[data-option-update]", function () {
            var id = $(this).attr('data-option-update');
            var title = $('#option-' + id).val();
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/update/option',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id,
                    title: title
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-option-table]').empty();
                        $('[data-option-table]').append(result.data);
                        sortOption();
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });

    }
    var valueUpdate = function () {

        $(document).on("click", "[data-option-value-update]", function () {
            var id = $(this).attr('data-option-value-update');
            var text = $(this).attr('data-option-value');
            var old = $(this).attr('data-option-value-old');
            var title = $('#' + text).val();
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/update/value',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id,
                    old: old,
                    title: title
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-option-table]').empty();
                        $('[data-option-table]').append(result.data);
                        sortOption();
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });

    }
    var optionCreate = function () {

        $(document).on("click", "[data-option-create]", function () {
            var type = $(this).attr('data-option-create');
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/create/option',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    type: type,
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-option-table]').empty();
                        $('[data-option-table]').append(result.data);
                        sortOption();
                        $('html,body').animate({scrollTop: 9999}, 'slow');
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });

    }
    var valueCreate = function () {

        $(document).on("click", "[data-option-value-create]", function () {
            var id = $(this).attr('data-option-value-create');
            var text = $(this).attr('data-option-value');
            var title = $('#' + text).val();
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/create/value',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id,
                    title: title
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-option-table]').empty();
                        $('[data-option-table]').append(result.data);
                        sortOption();
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });

    }
    var showHide = function () {
        $(document).on("click", "[data-table-type]", function () {
            var type = $(this).attr('data-table-type');
            var _this = $(this);
            var neworold = (type == 0 ? 'old' : 'new');
            $.ajax({
                url: '/account/table/show',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    type: neworold,
                    status: 1,
                },
                cache: false,
                success: function (result) {
                    console.log(neworold);
                    if(neworold == 'new'){
                        $('[data-table-create="new"]').show();
                        $('[data-table-lastedit="new"]').show();
                        $('[data-table-search="new"]').show();
                        $('[data-table-new-show]').attr('data-table-new-show',1);
                        $('[data-table-new-show]').show();
                        $('#table-new_wrapper').css({'opacity': 1,'height': '100%','z-index':'1'});
                        $('#table-new').css({'opacity': 1,'z-index':'1'});

                        $('[data-table-create="old"]').hide();
                        $('[data-table-lastedit="old"]').hide();
                        $('[data-table-search="old"]').hide();
                        $('[data-table-old-show]').attr('data-table-old-show',0);
                        $('[data-table-old-show]').hide();

                        $('#table-old_wrapper').css({'opacity': 0,'height': '0px','z-index':'8000'});
                        $('#table-old').css({'opacity': 0,'z-index':'8000'});
                    }
                    if(neworold == 'old'){
                        $('[data-table-create="old"]').show();
                        $('[data-table-lastedit="old"]').show();
                        $('[data-table-search="old"]').show();
                        $('[data-table-old-show]').show();
                        $('[data-table-old-show]').attr('data-table-old-show',1);
                        $('#table-old_wrapper').css({'opacity': 1,'height': '100%','z-index':'1'});
                        $('#table-old').css({'opacity': 1,'z-index':'1'});

                        $('[data-table-create="new"]').hide();
                        $('[data-table-lastedit="new"]').hide();
                        $('[data-table-search="new"]').hide();
                        $('[data-table-new-show]').hide();
                        $('[data-table-new-show]').attr('data-table-new-show',0);
                        $('#table-new_wrapper').css({'opacity': 0,'height': '0px','z-index':'8000'});
                        $('#table-new').css({'opacity': 0,'z-index':'8000'});
                    }
                }
            });
        });
    }
    var valueDelete = function () {

        $(document).on("click", "[data-option-value-delete]", function () {
            var id = $(this).attr('data-option-value-delete');
            var text = $(this).attr('data-option-value');
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/settings/delete/value',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    id: id,
                    title: text
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-option-table]').empty();
                        $('[data-option-table]').append(result.data);
                        sortOption();
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });
    }

    var tableCreate = function () {

        $(document).on("click", "[data-table-create]", function () {
            var old = $(this).attr('data-table-create');
            var $this = $(this);
            var oldbtn = $this.html();
            $this.html(loader);

            $.ajax({
                url: '/account/table/create',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    old: old,
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-table-table]').empty();
                        $('[data-table-table]').append(result.data);
                        adattable();
                    } else {
                        toastr.error(result.message);
                    }
                    $this.html(oldbtn);
                }
            });
        });
    }
    var changeYear = function () {
        $(document.body).on('change', "[data-table-changeyear]", function (e) {
            var year = $(this).find(":selected").val();

            $.ajax({
                url: '/account/table/changeyear',
                type: "POST",
                method: "POST",
                dataType: "json",
                data: {
                    year: year,
                },
                cache: false,
                success: function (result) {
                    if (result.status == 'success') {
                        toastr.success(result.message);
                        $('[data-table-table]').empty();
                        $('[data-table-table]').append(result.data);
                        adattable();

                    } else {
                        toastr.error(result.message);
                    }
                }
            });
        });
    }
    var tableYear = function () {

        $(document).on("click", "[data-table-year]", function () {
            var r = confirm("Biztosan hozzá szeretne adni egy új évet?");
            if (r == true) {

                var $this = $(this);
                var oldbtn = $this.html();
                $this.html(loader);

                $.ajax({
                    url: '/account/table/year',
                    type: "POST",
                    method: "POST",
                    dataType: "json",
                    data: {
                        year: '1',
                    },
                    cache: false,
                    success: function (result) {
                        if (result.status == 'success') {
                            toastr.success(result.message);
                            $('[data-table-table]').empty();
                            $('[data-table-table]').append(result.data);
                            adattable();

                        } else {
                            toastr.error(result.message);
                        }
                        $this.html(oldbtn);
                    }
                });
            }
        });

    }
    var tableDelete = function () {

        $(document).on("click", "[data-table-delete]", function () {
            var r = confirm("Biztosan törölni szeretné ezt a sort?");
            if (r == true) {
                var id = $(this).attr('data-table-delete');
                var $this = $(this);
                var oldbtn = $this.html();
                $this.html(loader);

                $.ajax({
                    url: '/account/table/delete',
                    type: "POST",
                    method: "POST",
                    dataType: "json",
                    data: {
                        id: id,
                    },
                    cache: false,
                    success: function (result) {
                        if (result.status == 'success') {
                            toastr.success(result.message);
                            $('[data-table-table]').empty();
                            $('[data-table-table]').append(result.data);
                            adattable();

                        } else {
                            toastr.error(result.message);
                        }
                        $this.html(oldbtn);
                    }
                });
            }
        });
    }

    var edit = function () {
        $(document).on("click", "td.datatext", function (e) {

            e.stopPropagation();
            $(".editable-container").each(function (index) {
                $(this).remove();
            });
            $(".editable").each(function (index) {
                $(this).editable("destroy");
                $(this).show();
                $(this).removeClass('editable');
                $(this).removeClass('editable-click');
                $(this).removeClass('editable-open');
            });


            $(this).find('.datatext').editable({
                type: 'text',
                url: '/account/table/update',
                emptytext: '0',
                params: function (params) { //params already contain `name`, `value` and `pk`
                    var data = {};
                    data['id'] = params.pk;
                    data['field'] = $(this).editable().data('field');
                    data['value'] = params.value;
                    data['name'] = params.name;
                    data['option_id'] = $(this).editable().data('option');
                    return data;
                },
                title: '',
                success: function (result, newValue) {
                    var table = $(this).closest('.dataTables_wrapper').attr('id');
                    var srcTableLeft =  $('#'+table).find('.dataTables_scrollBody').scrollLeft();
                    var srcTabletop =   $('#'+table).find('.dataTables_scrollBody').scrollTop();
                    console.log('left',srcTableLeft);
                    console.log('top',srcTableLeft);
                    toastr.success(result.message);
                    $('[data-table-table]').empty();
                    $('[data-table-table]').append(result.data);
                    adattable();
                    $('#'+table).find('.dataTables_scrollBody').scrollLeft(srcTableLeft);
                    $('#'+table).find('.dataTables_scrollBody').scrollTop(srcTabletop);
                }
            }).on('shown', function (ev, editable) {
                setTimeout(function () {
                    editable.input.$input.select();
                }, 0);
            });
            $(this).find('.datatext').editable('toggle');
        });
        $(document).on("click", "td.dataselect", function (e) {
            e.stopPropagation();
            $(".editable-container").each(function (index) {
                $(this).remove();
            });
            $(".editable").each(function (index) {
                $(this).editable("destroy");
                $(this).show();
                $(this).removeClass('editable');
                $(this).removeClass('editable-click');
                $(this).removeClass('editable-open');
            });
            $(this).find('.dataselect').editable({
                type: 'text',
                url: '/account/table/update',
                emptytext: '0',
                params: function (params, asd) {
                    var data = {};
                    data['id'] = params.pk;
                    data['field'] = $(this).editable().data('field');
                    data['value'] = params.value;
                    data['name'] = params.name;
                    data['option_id'] = $(this).editable().data('option');
                    return data;
                },
                title: '',
                success: function (result, newValue) {
                    toastr.success(result.message);
                    $('[data-table-table]').empty();
                    $('[data-table-table]').append(result.data);
                    adattable();
                }
            }).on('shown', function (ev, editable) {
                setTimeout(function () {
                    editable.input.$input.select();
                }, 0);
            });
            $(this).find('.dataselect').editable('toggle');
        });
    }
    var adattable = function () {
        dataDatatable();
    }
    return {
        init: function () {
            verifyToken();
            newemail();

            userAdd();
            userDel();
            userUpdate();

            placeAdd();
            placeDelete();
            placeUpdate();

            sortOption();
            optionDelete();
            optionUpdate();
            optionCreate();

            valueUpdate();
            valueCreate();
            valueDelete();


            changeOld();
            tableDelete();
            tableCreate();
            tableYear();

            changeYear();

            setDatatableDefaults();
            adattable();
            edit();
            showHide();
            showLog();
        }
    };
}();

$(function () {
    App.init();
});

