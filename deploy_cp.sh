#!/bin/sh
git fetch --all
git reset --hard origin/master
php artisan down
echo "Run Composer Install"
php artisan cache:clear
echo "Run migrations"
php artisan migrate --force
chown -R bamakomb:bamakomb .
chmod 777 -R storage
chmod -R guo+w storage
find . -type d -exec chmod 755 {} +
find . -type f -exec chmod 655 {} +
php artisan up
