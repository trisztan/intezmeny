<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use App\Models\{Product, Category, Page};
use Spatie\Sitemap\Tags\Url;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sitemap generálás';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->productSitemap();
        $this->categoriesSitemap();
    }

    public function categoriesSitemap()
    {
        $categories = Category::select('fullslug')->groupBy('fullslug');
        $count = $categories->count();
        $i = 0;
        foreach ($categories->get() as $key => $category) {
            $i++;
            if ($i == 1 || ($i % 10001 == 0)) {
                $number = intval($i / 10000) + 1;
                $sitemap = new Sitemap();
                $this->info($number . ' Sitemap generálás');
            }
            $this->info($i . ' link');
            $sitemap->add(Url::create($category->fullslug)->setPriority(0.8));
            if (($i % 10000 == 0) || $count == $i) {
                $number = intval($i / 10000);
                $this->info($number . ' sitemap mentve');
                $sitemap->writeToFile(public_path($number . '-categories-sitemap.xml'));
            }
        }
    }

    public function productSitemap()
    {
        $products = Product::select('fullslug');
        $count = $products->count();
        $i = 0;
        foreach ($products->get() as $key => $product) {
            $i++;
            if ($i == 1 || ($i % 10001 == 0)) {
                $number = intval($i / 10000) + 1;
                $sitemap = new Sitemap();
                $this->info($number . ' Sitemap generálás');
            }
            $this->info($i . ' link');
            $sitemap->add(Url::create($product->fullslug)->setPriority(0.8));
            if (($i % 10000 == 0) || $count == $i) {
                $number = intval($i / 10000);
                $this->info($number . ' sitemap mentve');
                $sitemap->writeToFile(public_path($number . '-products-sitemap.xml'));
            }
        }
    }

    public function pageSitemap()
    {
        $pages = Page::select('slug');

        $sitemap = new Sitemap();
        $this->info('Page Sitemap generálás');
        foreach ($pages->get() as $key => $page) {
            $sitemap->add(Url::create($page->slug)->setPriority(0.8));
        }
        $this->info('Page Sitemap mentve');
        $sitemap->writeToFile(public_path('page-sitemap.xml'));
    }
}
