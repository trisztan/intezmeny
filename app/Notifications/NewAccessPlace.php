<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class NewAccessPlace extends Notification
{
    public $place;
    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($place,$user)
    {
        $this->place = $place;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Új intézmény hozzáférés')
            ->from(config('mail.from.address'),config('app.name'))
            ->greeting('Kedves,'.$this->user->email.'!')
            ->line('Ön hozzáférést kapott a(z) '.$this->place.' intézményhez!')
            ->action('Bejelentkezés', url('/'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
