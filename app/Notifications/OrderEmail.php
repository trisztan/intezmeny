<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\OrderTemplates;

class OrderEmail extends Notification
{
    public $template;
    public $order;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($templateId,$order)
    {
        $this->template = OrderTemplates::find($templateId);
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->from(config('mail.from.address'),'🔧 Motorpatika.hu')
                    ->greeting('Kedves '.$this->order->lastname.' '.$this->order->firstname)
                    ->line('Kövesse rendelése állapotát a "Nyomonkövetés" gomb segítségével.')
                    ->subject('⚡📦 Rendelés: '.($this->template->email_title ?? ''))
                    ->action('Nyomonkövetés', url('megrendeles/allapot/'.$this->order->check_id))
                    ->line(($this->template->email_body ?? ''))
                    ->line($this->template());
    }

    public function template() {
        return view('backend.mail.order',['order'=>$this->order])->render();
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
