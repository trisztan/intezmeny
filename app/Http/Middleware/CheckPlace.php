<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPlace
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return route('login');
        }
        if ($request->is('account/place/*')) {
            return $next($request);
        }
        if (session()->has('place')) {
            return $next($request);
        }
        return redirect('account/places');
    }
}
