<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\PlaceUser;
use App\Models\Place;
use Auth;
use App\Notifications\NewUserEmail;
use App\Notifications\NewAccessPlace;

class UserController extends Controller
{
    public function deleteUser(Request $request)
    {
        PlaceUser::whereUserId($request->id)->wherePlaceId(session('place'))->delete();

        $placeUsers = Place::find(session('place'))->users;
        $roles = PlaceUser::getRoles();

        return response()->json(['status'  => 'success',
                                 'message' => 'A felhasználó törölve!',
                                 'data'    => view('backend.settings.partials.user',
                                     compact('placeUsers', 'roles'))->render()
        ]);
    }

    public function updateUser(Request $request)
    {
        $place = PlaceUser::whereUserId($request->id)->wherePlaceId(session('place'))->first();
        $place->level = $request->level;
        $place->update();

        $placeUsers = Place::find(session('place'))->users;
        $roles = PlaceUser::getRoles();

        return response()->json(['status'  => 'success',
                                 'message' => 'A felhasználó jogosultság frissítve!',
                                 'data'    => view('backend.settings.partials.user',
                                     compact('placeUsers', 'roles'))->render()
        ]);
    }

    public function createUser(Request $request)
    {
        $user = User::whereEmail($request->email)->first();
        $user_id = $user->id ?? false;

        if (!$user_id) {
            $password = str_random('6');

            if (!filter_var($request->email, FILTER_VALIDATE_EMAIL) || empty($request->email)) {
                return response()->json(['status' => 'error', 'message' => 'Hibás e-mail cím formátum!']);
            }

            $user = new User;
            $user->email = $request->email;
            $user->password = $password;
            $user->is_admin = $request->level == 2 ? 1 : 0;
            $user->save();

            $user_id = $user->id;

            $user->notify(new NewUserEmail($password, $user));
        }

        $checkExist = PlaceUser::whereUserId($user_id)->wherePlaceId(session('place'));

        if ($checkExist->count() > 0) {
            return response()->json(['status' => 'error', 'message' => 'A felhasználó már korábban hozzá lett adva!']);
        }

        $placeUser = new PlaceUser;
        $placeUser->level = $request->level;
        $placeUser->place_id = session('place');
        $placeUser->user_id = $user_id;
        $placeUser->save();

        $place = Place::find(session('place'));
        $placeUsers = $place->users;
        $place = $place->title;

        $user->notify(new NewAccessPlace($place, $user));
        $roles = PlaceUser::getRoles();

        return response()->json(['status'  => 'success',
                                 'message' => 'A felhasználó hozzáadva!',
                                 'data'    => view('backend.settings.partials.user',
                                     compact('placeUsers', 'roles'))->render()
        ]);
    }
}
