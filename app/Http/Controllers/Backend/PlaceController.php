<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Place;
use App\Models\PlaceUser;
use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Payment;
use App\Models\Option;

class PlaceController extends Controller
{

    public function place()
    {
        $places = User::find(Auth::user()->id)->places;
        return view('backend.place', compact('places'));
    }

    public function selectPlace(Request $request)
    {
        session(['year' => date('Y')]);
        session(['place' => $request->id]);
        return redirect('/account');
    }

    public function createPlace(Request $request)
    {
        if (empty($request->title)) {
            return response()->json(['status' => 'error', 'message' => 'Az intézmény neve kötelező!']);
        }

        $place = new Place;
        $place->title = $request->title;
        $place->save();

        $userPlace = new PlaceUser;
        $userPlace->user_id = Auth::user()->id;
        $userPlace->place_id = $place->id;
        $userPlace->level = 2;
        $userPlace->save();

        $newpay = new Payment;
        $newpay->year = date('Y');
        $newpay->name = 'Év';
        $newpay->is_year = 1;
        $newpay->old = 0;
        $newpay->last_edit = Auth::user()->id;
        $newpay->place_id = $place->id;
        $newpay->save();

        $newpay = new Payment;
        $newpay->year = date('Y')-1;
        $newpay->name = 'Év';
        $newpay->is_year = 1;
        $newpay->old = 0;
        $newpay->last_edit = Auth::user()->id;
        $newpay->place_id = $place->id;
        $newpay->save();

        $options = Option::wherePlaceId(session('place'));
        if ($options->count() > 0) {
            foreach ($options->get() as $opt) {
                $option = new Option();
                $option->option = $opt->option;
                $option->type = $opt->type;
                $option->values = $opt->values;
                $option->place_id = $place->id;
                $option->order_id = $opt->order_id;
                $option->save();
            }
        }


        $places = Place::get();

        return response()->json(['status'  => 'success',
                                 'message' => 'Az intézmény hozzáadva!',
                                 'data'    => view('backend.settings.partials.place', compact('places'))->render()
        ]);
    }

    public function updatePlace(Request $request)
    {
        if (empty($request->title)) {
            return response()->json(['status' => 'error', 'message' => 'Az intézmény neve kötelező!']);
        }

        $place = Place::find($request->id);
        $place->title = $request->title;
        $place->save();

        $places = Place::get();

        return response()->json(['status'  => 'success',
                                 'message' => 'Az intézmény neve frissítve!',
                                 'data'    => view('backend.settings.partials.place', compact('places'))->render()
        ]);
    }

    public function deletePlace(Request $request)
    {
        Place::find($request->id)->delete();
        PlaceUser::wherePlaceId($request->id)->delete();

        $places = Place::get();

        return response()->json(['status'  => 'success',
                                 'message' => 'Az intézmény és minden adata törölve lett!',
                                 'data'    => view('backend.settings.partials.place', compact('places'))->render()
        ]);
    }
}
