<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Option;
use App\Models\OptionValues;
use App\Models\Table;
use Carbon\Carbon;
use App\Models\PaymentLog;

class AdminController extends Controller
{

    public function login()
    {
        session()->forget('year');
        session()->forget('place');
        return view('backend.login');
    }

    public function show(Request $request)
    {
        if($request->type =='new'){
            Table::updateOrCreate([
                'place_id' => session('place'),
                'user_id'  => Auth::user()->id,
                'year'     => session('year'),
                'type'     => 'new'
            ], ['status' => 1]);
            Table::updateOrCreate([
                'place_id' => session('place'),
                'user_id'  => Auth::user()->id,
                'year'     => session('year'),
                'type'     => 'old'
            ], ['status' => 0]);
        }
        else {
            Table::updateOrCreate([
                'place_id' => session('place'),
                'user_id'  => Auth::user()->id,
                'year'     => session('year'),
                'type'     => 'old'
            ], ['status' => 1]);
            Table::updateOrCreate([
                'place_id' => session('place'),
                'user_id'  => Auth::user()->id,
                'year'     => session('year'),
                'type'     => 'new'
            ], ['status' => 0]);
        }
        return response()->json(['success' => true]);
    }

    public function update(Request $request)
    {
        if ($request->has('option_id')) {
            $optionValues = OptionValues::wherePaymentId($request->id)->whereOptionId($request->option_id);

            if ($optionValues->count() > 0) {
                $optionValues = $optionValues->first();
            } else {
                $optionValues = new OptionValues();
            }
            $optionValues->payment_id = $request->id;
            $optionValues->value = $request->value ?? 0;
            $optionValues->option_id = $request->option_id;
            $optionValues->year = session('year');
            $optionValues->save();
        } else {
            $option = Payment::find($request->id);
            $option->{$request->name} = $request->value ?? 0;
            $option->update();
        }

        PaymentLog::create([
            'year'       => session('year'),
            'place_id'   => session('place'),
            'name'       => $request->field ?? 0,
            'value'      => $request->value ?? 0,
            'user'       => Auth::user()->id,
            'email'      => Auth::user()->email,
            'payment_id' => $request->id,
        ]);

        $option = Payment::find($request->id);
        $option->last_edit = Auth::user()->id;
        $option->updated_at = Carbon::now();
        $option->update();


        $year = session('year') ?? date('Y');

        $payments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 0)
            ->orderBy('name')
            ->get();
        $oldpayments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 1)
            ->orderBy('name')
            ->get();
        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();
        $years = Payment::select('year')
                ->wherePlaceId(session('place'))
                ->groupBy('year')
                ->where('is_year', 1)
                ->get() ?? null;

        return response()->json([
            'status'  => 'success',
            'message' => 'Érték módosítva',
            'data'    => view('backend.partials.main', compact('payments', 'oldpayments', 'options', 'years'))->render()
        ]);
    }

    public function log(Request $request)
    {
        $logs = PaymentLog::wherePaymentId($request->id)->get();
        return response()->json(['html' => view('backend.partials.log', compact('logs'))->render()]);
    }

    public function yearChange(Request $request)
    {
        session(['year' => $request->year]);
        $year = session('year') ?? date('Y');
        $payments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 0)
            ->orderBy('name')
            ->get();
        $oldpayments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 1)
            ->orderBy('name')
            ->get();
        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();
        $years = Payment::select('year')
                ->wherePlaceId(session('place'))
                ->groupBy('year')
                ->where('is_year', 1)
                ->get() ?? null;

        return response()->json([
            'status'  => 'success',
            'message' => $year . ' kiválasztva!',
            'data'    => view('backend.partials.main', compact('payments', 'oldpayments', 'options', 'years'))->render()
        ]);
    }

    public function year(Request $request)
    {
        $max_id = Payment::whereIsYear(1)
                ->wherePlaceId(session('place'))
                ->orderBy('year', 'desc')
                ->first()->year ?? date('Y');
        $max_id = $max_id + 1;

        $payment = new Payment;
        $payment->year = $max_id;
        $payment->old = ($request->old == 'old' ? 1 : 0);
        $payment->name = 'Új sor';
        $payment->place_id = session('place');
        $payment->last_edit = Auth::user()->id;
        $payment->is_year = 1;
        $payment->save();

        $year = session('year') ?? date('Y');
        $payments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 0)
            ->orderBy('name')
            ->get();

        $paymentsNew = Payment::whereOld(0)
            ->whereIsYear(0)
            ->where('year', session('year'))
            ->wherePlaceId(session('place'));
        $optionValues = OptionValues::where('year', session('year'))->wherePlaceId(session('place'));

        if ($paymentsNew->count() > 0) {
            foreach ($paymentsNew->get() as $pay) {
                $newpay = new Payment;
                $newpay->year = $max_id;
                $newpay->name = $pay->name;
                $newpay->is_year = $pay->is_year;
                $newpay->old = 0;
                $newpay->last_edit = Auth::user()->id;
                $newpay->place_id = session('place');
                $newpay->save();

                $optionValues = OptionValues::wherePaymentId($pay->id);
                if ($optionValues->count() > 0) {
                    foreach ($optionValues->get() as $val) {
                        $newValue = new OptionValues;
                        $newValue->option_id = $val->option_id;
                        $newValue->payment_id = $newpay->id;
                        $newValue->value = $val->value;
                        $newValue->year = $max_id;
                        $newValue->save();
                    }
                }
            }
        }
        /*OptionValues::wherePaymentId($payment->id)->where('year',session('year'))->whereOptionId($option->id)->first() ?? '
*/
        $oldpayments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 1)
            ->orderBy('name')
            ->get();

        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();

        $years = Payment::select('year')
                ->wherePlaceId(session('place'))
                ->groupBy('year')
                ->where('is_year', 1)
                ->get() ?? null;

        return response()->json([
            'status'  => 'success',
            'message' => $max_id . ' hozzáadva!',
            'data'    => view('backend.partials.main', compact('payments', 'oldpayments', 'options', 'years'))->render()
        ]);
    }

    public function create(Request $request)
    {
        $payment = new Payment;
        $payment->year = session('year') ?? date('Y');
        $payment->old = ($request->old == 'old' ? 1 : 0);
        $payment->name = 'Új sor';
        $payment->place_id = session('place');
        $payment->last_edit = Auth::user()->id;
        $payment->is_year = 0;
        $payment->save();

        $year = session('year') ?? date('Y');
        $payments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 0)
            ->orderBy('name')
            ->get();
        $oldpayments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 1)
            ->orderBy('name')
            ->get();
        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();
        $years = Payment::select('year')
                ->wherePlaceId(session('place'))
                ->groupBy('year')
                ->where('is_year', 1)
                ->get() ?? null;

        return response()->json([
            'status'  => 'success',
            'message' => 'Új sor hozzáadva!',
            'data'    => view('backend.partials.main', compact('payments', 'oldpayments', 'options', 'years'))->render()
        ]);
    }

    public function delete(Request $request)
    {
        $payment = Payment::find($request->id)->delete();
        $values = OptionValues::wherePaymentId($request->id)->delete();

        $year = session('year') ?? date('Y');
        $payments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 0)
            ->orderBy('name')
            ->get();
        $oldpayments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 1)
            ->orderBy('name')
            ->get();
        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();
        $years = Payment::select('year')
                ->wherePlaceId(session('place'))
                ->groupBy('year')
                ->where('is_year', 1)
                ->get() ?? null;

        return response()->json([
            'status'  => 'success',
            'message' => 'Sor törölve!',
            'data'    => view('backend.partials.main', compact('payments', 'oldpayments', 'options', 'years'))->render()
        ]);
    }

    public function changeOld(Request $request)
    {
        $payment = Payment::find($request->id);

        if ($payment->old == 0) {
            $payment->old = 1;
        } else {
            $payment->old = 0;
        }
        $payment->update();

        $year = session('year') ?? date('Y');
        $payments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 0)
            ->orderBy('name')
            ->get();
        $oldpayments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 1)
            ->orderBy('name')
            ->get();
        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();
        $years = Payment::select('year')
                ->wherePlaceId(session('place'))
                ->groupBy('year')
                ->where('is_year', 1)
                ->get() ?? null;

        return response()->json([
            'status'  => 'success',
            'message' => 'Az állapot megváltoztatva!',
            'data'    => view('backend.partials.main', compact('payments', 'oldpayments', 'options', 'years'))->render()
        ]);
    }

    public function index()
    {
        $year = session('year') ?? date('Y');
        $payments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 0)
            ->orderBy('name')
            ->get();
        $oldpayments = Payment::wherePlaceId(session('place'))
            ->where('is_year', 0)
            ->where('year', $year)
            ->where('old', 1)
            ->orderBy('name')
            ->get();
        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();
        $years = Payment::select('year')
                ->wherePlaceId(session('place'))
                ->groupBy('year')
                ->where('is_year', 1)
                ->get() ?? null;
        return view('backend.index', compact('payments', 'oldpayments', 'options', 'years'));
    }
}
