<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Option;
use App\Models\OptionValues;

class OptionController extends Controller
{

    public function orderOption(Request $request)
    {
        foreach ($request->order as $order => $optionId) {
            Option::find($optionId)->update(['order_id' => $order + 1]);
        }

        return response()->json(['status' => 'success', 'message' => 'Pozició mentve!']);
    }

    public function deleteOption(Request $request)
    {
        Option::find($request->id)->delete();
        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();
        // @todo értékek törlése is
        return response()->json([
            'status'  => 'success',
            'message' => 'Opció törölve!',
            'data'    => view('backend.settings.partials.option', compact('options'))->render()
        ]);
    }

    public function updateOption(Request $request)
    {
        if (empty($request->title)) {
            return response()->json(['status' => 'error', 'message' => 'Az mező neve kötelező']);
        }

        $place = Option::find($request->id);
        $place->option = $request->title;
        $place->save();

        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();

        return response()->json([
            'status'  => 'success',
            'message' => 'Opció frissítve!',
            'data'    => view('backend.settings.partials.option', compact('options'))->render()
        ]);
    }

    public function createOption(Request $request)
    {
        $max_id = Option::wherePlaceId(session('place'))->orderBy('order_id', 'desc')->first()->order_id ?? 0;
        $option = new Option();
        $option->option = 'Új mező';
        $option->type = $request->type;
        $option->place_id = session('place');
        $option->order_id = $max_id + 1;
        $option->save();

        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();

        return response()->json([
            'status'  => 'success',
            'message' => 'Mező hozzáadva!',
            'data'    => view('backend.settings.partials.option', compact('options'))->render()
        ]);
    }

    public function deleteOptionValue(Request $request)
    {
        Option::find($request->id)->delete();
        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();
        // @todo értékek törlése is
        return response()->json([
            'status'  => 'success',
            'message' => 'Opció törölve!',
            'data'    => view('backend.settings.partials.option', compact('options'))->render()
        ]);
    }

    public function updateValue(Request $request)
    {
        $option = Option::find($request->id);

        $values = explode(',', $option->values);

        $newvalues = [];

        foreach ($values as $value) {
            if ($value == $request->old) {
                $newvalues[] = $request->title;

            } else {
                $newvalues[] = $value;
            }
        }

        $option->values = implode(',', $newvalues);
        $option->save();

        $values = OptionValues::whereOptionId($request->id)
            ->where('value',$request->old)
            ->update(['value' => $request->title]);
        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();

        return response()->json([
            'status'  => 'success',
            'message' => 'Opció mentve!',
            'data'    => view('backend.settings.partials.option', compact('options'))->render()
        ]);
    }

    public function createValue(Request $request)
    {
        $option = Option::find($request->id);
        $values = explode(',', $option->values);

        $newvalues = [];
        $checkEmpty = false;

        if (count($values) > 1) {
            foreach ($values as $value) {
                if ($value == 'Üres') {
                    $checkEmpty = true;
                }
                $newvalues[] = $value;
            }
        } else {
            $newvalues[] = $option->values;
        }

        $newtitle = $request->title ?? 'Üres';

        if ($checkEmpty == true && $newtitle == 'Üres') {
            return response()->json([
                'status'  => 'error',
                'message' => 'Több üres mezőt nem adhatsz hozzá!',
            ]);
        }

        $newvalues[] = $newtitle;
        $option->values = implode(',', $newvalues);
        $option->update();

        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();
        $values = OptionValues::whereOptionId($request->id)
            ->where('value',$request->old)
            ->update(['value' => $request->title]);
        
        return response()->json([
            'status'  => 'success',
            'message' => 'Opció mentve!',
            'data'    => view('backend.settings.partials.option', compact('options'))->render()
        ]);
    }
    public function deleteValue(Request $request)
    {
        $option = Option::find($request->id);
        $values = explode(',', $option->values);

        $newvalues = [];

        if (count($values) > 1) {
            foreach ($values as $value) {
                if ($value != $request->title) {
                    $newvalues[] = $value;
                }
            }
            $newvalues = implode(',', $newvalues);
        } else {
            $newvalues = "";
        }

        $option->values = $newvalues;
        $option->update();

        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();

        return response()->json([
            'status'  => 'success',
            'message' => 'Opció mentve!',
            'data'    => view('backend.settings.partials.option', compact('options'))->render()
        ]);
    }
}
