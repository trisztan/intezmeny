<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Place;
use App\Models\PlaceUser;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use App\Models\Option;

class SettingController extends Controller
{
    /**
     * Show users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $users = User::select('email')->get()->pluck('email', 'email');
        $placeUsers = Place::find(session('place'))->users;
        $roles = PlaceUser::getRoles();
        $places = Place::get();
        $options = Option::wherePlaceId(session('place'))->orderBy('order_id')->get();
        if(Auth::user()->rank != 'ADMIN'){
            abort('404');
        }
        return view('backend.settings.form', compact('roles', 'placeUsers', 'users', 'places','options'));
    }
}
