<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use App\Notifications\ResetPassword;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    const VIEW = 0;
    const EDIT = 1;
    const ADMIN = 2;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = ['email', 'password'];

    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $appends = [
        'rank',
    ];
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    /**
     * Set password hash
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
    }

    public function places()
    {
        return $this->belongsToMany('App\Models\Place');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token,$this->email));
    }

    public function setLvlAttribute()
    {
        $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
    }

    /**
     * Get diff from now to started_at
     *
     * @return int
     */
    public function getRankAttribute()
    {
       $rank = PlaceUser::whereUserId($this->id)->wherePlaceId(session('place'))->first()->level ?? 0;
       return self::getRoles()[$rank];
    }
    /*
  |--------------------------------------------------------------------------
  | STATIC FUNCTIONS
  |--------------------------------------------------------------------------
  */
    /**
     * @return array
     */
    public static function getRoles()
    {
        return [
            self::VIEW  => 'VIEW',
            self::EDIT  => 'EDIT',
            self::ADMIN  => 'ADMIN',
        ];
    }
}
