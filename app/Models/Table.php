<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $fillable = [
        'place_id',
        'year',
        'user_id',
        'type',
        'status'
    ];
}
