<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('level');
    }
}
