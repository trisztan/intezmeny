<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentLog extends Model
{
    protected $table = 'payments_log';
    protected $fillable = [
        'year',
        'place_id',
        'payment_id',
        'name',
        'value',
        'user',
        'email',
        'old'
    ];
}
