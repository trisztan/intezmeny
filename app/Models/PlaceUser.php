<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceUser extends Model
{
    protected $table = 'place_user';

    const VIEW = 0;
    const EDIT = 1;
    const ADMIN = 2;
    /*
    |--------------------------------------------------------------------------
    | STATIC FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /**
     * @return array
     */
    public static function getRoles()
    {
        return [
            self::VIEW  => 'Megtekintés',
            self::EDIT  => 'Szerkesztés',
            self::ADMIN  => 'Admin',
        ];
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    /**
     * @return mixed
     */
    public function getWidgetLabelAttribute()
    {
        return self::getRoles()[$this->level];
    }
}
