<?php

namespace App\Observers;

use App\Payment;

class PaymentObserver
{
    /**
     * Handle the payment "created" event.
     *
     * @param  \App\Payment  $payment
     * @return void
     */
    public function created(Payment $payment)
    {
        $payment->last_edit = Auth::user()->id;
        $payment->updated_at = date('Y-m-d  H:i:s');
    }

    /**
     * Handle the payment "updated" event.
     *
     * @param  \App\Payment  $payment
     * @return void
     */
    public function updated(Payment $payment)
    {
        $payment->last_edit = Auth::user()->id;
        $payment->updated_at = date('Y-m-d  H:i:s');
    }
}
